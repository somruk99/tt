import { Injectable } from '@angular/core';
import { WebsocketServiceService } from './websocket-service.service';
import { from,Subject,Observable,Observer } from 'rxjs';
import { map } from 'rxjs/operators';
const URL = "ws://127.0.0.1:8080/";
@Injectable({
  providedIn: 'root'
})



export class DataServiceService {

  public messages: Subject<any>;
  
  constructor(
      private wsService: WebsocketServiceService,

    ){
      this.messages = <Subject<any>>wsService.connect(URL).pipe(map(
        (response: MessageEvent): any => {
          let data = JSON.parse(response.data);
          console.log(data)
          return {
            author: data.author,
            message: data.message,
            value: data.value
          };
        }
      ));
}
}
